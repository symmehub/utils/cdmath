#!/bin/bash
echo "Start Devcontainer post-command process !"

cd ../workspace/
echo "Installing pre-commit"
echo $CONDA_ENV
${CONDA_ENV}pre-commit install
${CONDA_ENV}pre-commit install-hooks
echo "Devcontainer post-command process done !"
