import math
import sys

import numpy as np
from numba import cuda


@cuda.jit(device=True)
def cuda_mat_add(A, B, M, dim):
    """_summary_
    Add two matrices of dimension (m,n).
    A, B, M are (m,n) arrays.
    Args:
        A (array): _description_
        B (array): _description_
        M (array): _description_
        dim (tuple): (m,n)
    """
    for i in range(dim[0]):
        for j in range(dim[1]):
            M[i, j] = A[i, j] + B[i, j]


@cuda.jit(device=True)
def cuda_mat_sub(A, B, M, dim):
    """_summary_
    Subtract two matrices of dimension (m,n).
    A, B, M are (m,n) arrays.
    Args:
        A (array): _description_
        B (array): _description_
        M (array): _description_
        dim (tuple): (m,n)
    """
    for i in range(dim[0]):
        for j in range(dim[1]):
            M[i, j] = A[i, j] - B[i, j]


@cuda.jit(device=True)
def cuda_mat_scalarDiv(A, s, M, dim):
    """_summary_
    Divide a matrix by a scalar.
    A, M are (m,n) arrays.
    Args:
        A (array): _description_
        s (float): _description_
        M (array): _description_
        dim (tuple): (m,n)
    """
    for i in range(dim[0]):
        for j in range(dim[1]):
            M[i, j] = A[i, j] / s


@cuda.jit(device=True)
def cuda_mat_scalarMul(A, s, M, dim):
    """_summary_
    Multiply a matrix by a scalar.
    A, M are (m,n) arrays.
    Args:
        A (array): _description_
        s (float): _description_
        M (array): _description_
        dim (tuple): (m,n)
    """
    for i in range(dim[0]):
        for j in range(dim[1]):
            M[i, j] = A[i, j] * s


@cuda.jit(device=True)
def cuda_mat_mul(A, B, M, dimA, dimB):
    """_summary_
    Return the matrix product of A(m, n) and B(n, p) into M(m,p).
    Args:
        A (array): _description_
        B (array): _description_
        M (array): _description_
        dimA (tuple): (m, n)
        dimB (tuple): (n, p)
    """

    rA = dimA[0]
    cA = dimA[1]
    rB = dimB[0]
    cB = dimB[1]
    if cA == rB:
        for i in range(rA):
            for j in range(cB):
                for k in range(cA):
                    M[i, j] += A[i, k] * B[k, j]


@cuda.jit(device=True)
def cuda_mat_mul_hadamard(A, B, M, dim):
    """_summary_
    Multiply two matrices of dimension (m,n) element-wise.
    A, B, M are (m,n) arrays.
    Args:
        A (array): _description_
        B (array): _description_
        M (array): _description_
        dim (tuple): (m,n)
    """
    for i in range(dim[0]):
        for j in range(dim[1]):
            M[i, j] = A[i, j] * B[i, j]


@cuda.jit(device=True)
def cuda_mat_vecMul(A, u, v, dim):
    """_summary_
    Return the matrix product of A(m, n) and u(n) into v(n).
    u is consider to be a column vector. So size of u must be as same as A columns.
    Args:
        A (array): input matrix
        u (vector): input vector
        v (vector): output vector
        dim (tuple): (m,n)
    """
    rA = dim[0]
    cA = dim[1]
    # if cA == len(u):
    for i in range(rA):
        for j in range(cA):
            v[i] += A[i, j] * u[j]


@cuda.jit(device=True)
def cuda_mat_transpose(M, M_T, dim):
    """_summary_
    Transpose a matrix M (m,n) to MT (n,m)
    Args:
        M (array): _description_
        MT (array): _description_
        dim (tuple): (m, n)
    """
    r = dim[0]
    c = dim[1]

    for i in range(r):
        for j in range(c):
            M_T[j, i] = M[i, j]


@cuda.jit(device=True)
def cuda_mat_ones(M, val, dim):
    """_summary_
    Set all elements of a matrix to val.
    Args:
        M (array): _description_
        val (float): _description_
        dim (tuple): (m, n)
    """
    r = dim[0]
    c = dim[1]

    for i in range(r):
        for j in range(c):
            M[i, j] = val

    return M


@cuda.jit(device=True)
def cuda_mat_ones(M, dim):
    """_summary_
    Set all elements of a matrix to 1
    Args:
        M (array): _description_
        dim (tuple): (m, n)
    """
    M = cuda_mat_ones(M, 1.0, dim)
    return M


@cuda.jit(device=True)
def cuda_mat_zeros(M, dim):
    """_summary_
    Set all elements of a matrix to 0
    Args:
        M (array): _description_
        dim (tuple): (m, n)
    """
    M = cuda_mat_ones(M, 0.0, dim)
    return M


@cuda.jit(device=True)
def cuda_eye(I, n):
    """
    Returns a n x n identity matrix.
    """
    for i in range(n):
        for j in range(n):
            I[i, j] = 1 if i == j else 0
    return I


@cuda.jit(device=True)
def cuda_mat_sum(M, s, axis, dim):
    """_summary_
    Sum the elements of a matrix along a given axis.
    Args:
        M (array): matrix to sum
        s (array): sum of elements along the given axis
        axis (int): axis along which to sum, must be 0 or 1
        dim (tuple): (m, n) Dimension of M.
    """
    r = dim[0]
    c = dim[1]
    if axis == 0:
        for i in range(r):
            for j in range(c):
                s[j] += M[i, j]
    elif axis == 1:
        for j in range(c):
            for i in range(r):
                s[i] += M[i, j]
    return s


@cuda.jit(device=True)
def cuda_mat_copy(M, N, dim):
    """_summary_
    Copy matrix M to N.
    Args:
        M (array): _description_
        N (array): _description_
        dim (tuple): (m, n)
    """
    r = dim[0]
    c = dim[1]
    for i in range(r):
        for j in range(c):
            N[i, j] = M[i, j]


@cuda.jit(device=True)
def cuda_mat_get_diag_vec(M, v, size):
    """_summary_
    Get the diagonal of a square matrix M as a vector v.

    Args:
        M (array): _description_
        v (array): _description_
        size (int): _description_
    """

    for i in range(size):
        for j in range(size):
            if i == j:
                v[i] = M[i, j]
