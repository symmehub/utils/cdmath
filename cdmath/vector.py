import math
import sys

import numpy as np
from numba import cuda

"""_summary_

# Convention
    Vector formalism 
     - Vectors will be always interpreted as a column vector.
 
"""


@cuda.jit(device=True)
def cuda_vec_norm(r, size):
    """_summary_

    Args:
        r (vecor): (1, r_dim) vector
        Ne (int): Number of elements in r

    Returns:
        _type_: _description_
    """
    n = 0.0
    for i in range(size):
        n += r[i] * r[i]
    return math.sqrt(n)


@cuda.jit(device=True)
def cuda_vec_get_normalized(v, n_v, size):
    """_summary_
    Returns a normalized vector.

    Args:
    v (vecor): (1, v_dim) vector to be normalized.
    n_v (vecor): (1, v_dim) container of normalized vector v.
    size (int): Number of elements in v.

    Returns:
        vector: _description_
    """
    n = cuda_vec_norm(v, size)
    if n > 0:
        for i in range(size):
            n_v[i] /= n


@cuda.jit(device=True)
def cuda_vec_add(U, V, W, size):
    """_summary_
    Add two vectors.
    Args:
        U (vector): input vector
        V (vector): input vector
        W (vector): output vector
        size (int):
    """
    for i in range(size):
        W[i] = U[i] + V[i]


@cuda.jit(device=True)
def cuda_vec_val(W, val, size):
    """_summary_
    Set all elements of a vector to val
    Args:
        W (array): _description_
        val (float): _description_
        size (int):
    """
    for i in range(size):
        W[i] = val


@cuda.jit(device=True)
def cuda_vec_zeros(W, size):
    """_summary_
    Set all elements of a vector to 0
    Args:
        W (array): _description_
        size (int):
    """
    W = cuda_vec_val(W, 0.0, size)


@cuda.jit(device=True)
def cuda_vec_ones(W, size):
    """_summary_
    Set all elements of a vector to 0
    Args:
        W (array): _description_
        size (int):
    """
    W = cuda_vec_val(W, 1.0, size)


@cuda.jit(device=True)
def cuda_vec_copy(W, V, size):
    """_summary_
    Copy vector V to W.
    Args:
        W (array): _description_
        V (array): _description_
        size (int):
    """
    for i in range(size):
        W[i] = V[i]

    return W


@cuda.jit(device=True)
def cuda_vec_scalarDiv(u, s, v, size):
    """_summary_
    Divide vector u by scalar s.
    Return the result in v.
    Args:
        u (array): Input vector
        s (float): Scalar
        v (array): Output vector
        size (int): Size of u and v
    """

    for i in range(size):
        v[i] = u[i] / s


@cuda.jit(device=True)
def cuda_vec_scalarMul(u, s, v, size):
    """_summary_
    Multiply vector u by scalar s.
    Return the result in v.
    Args:
        u (array): Input vector
        s (float): Scalar
        v (array): Output vector
        size (int): Size of u and v
    """
    for i in range(size):
        v[i] = u[i] * s


@cuda.jit(device=True)
def cuda_vec_scalarAdd(u, s, v, size):
    """_summary_
    Add scalar s to vector u.
    Return the result in v.
    Args:
        u (array): Input vector
        s (float): Scalar
        v (array): Output vector
        size (int): Size of u and v
    """
    for i in range(size):
        v[i] = u[i] + s


@cuda.jit(device=True)
def cuda_vec_scalar_prod(u, v, size):
    """_summary_
    Compute the scalar product of vectors u and v.
    Args:
        u (array): Input vector
        v (array): Input vector
        size (int): Size of u and v
    """
    prod = 0.0
    for i in range(size):
        prod += u[i] * v[i]
    return prod


@cuda.jit(device=True)
def cuda_vec_hadamard_prod(u, v, w, size):
    """_summary_
    Compute the hadamard product of vectors u and v.
    Args:
        u (vector): Input vector
        v (vector): Input vector
        w (vector): Output vector
        size (int): Size of u, v and w
    """
    for i in range(size):
        w[i] = u[i] * v[i]
