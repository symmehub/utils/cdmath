import cmath
import math
import sys
import numpy as np
from numba import cuda
import cdmath.matrix as cdmat
import cdmath.vector as cdvec
from math import pi, atan, sqrt, cos, sin


@cuda.jit(device=True)
def cuda_RotVec_To_RotMat(v, R, CONS_ZEROS, float_type):
    # sourcery skip: use-itertools-product
    """_summary_
    Convert Rodrigues rotation vector to rotation matrix.
    See : Tomasi, C., 2013. Vector representation of rotations. Computer Science, 527.

    Args:
        v (vector): (3) array, Rodrigues rotation vector
        R (matrix): (3,3) array, rotation matrix
        float_type (_type_): np.float64
    """

    I = cdmat.cuda_eye(cuda.local.array((3, 3), dtype=float_type), 3)

    r_rT = cuda.local.array((3, 3), dtype=float_type)
    r_cross = cuda.local.array((3, 3), dtype=float_type)
    r = cuda.local.array(3, dtype=float_type)

    theta = cdvec.cuda_vec_norm(v, 3)
    if theta < CONS_ZEROS:
        cdmat.cuda_mat_copy(I, R, (3, 3))
    else:
        for i in range(3):
            r[i] = v[i] / theta

        for i in range(3):
            for j in range(3):
                r_rT[i, j] = r[i] * r[j]
                if i == j:
                    r_cross[i, j] = 0

        r_cross[0, 1] = -r[2]
        r_cross[0, 2] = r[1]

        r_cross[1, 0] = r[2]
        r_cross[1, 2] = -r[0]

        r_cross[2, 0] = -r[1]
        r_cross[2, 1] = r[0]

        for i in range(3):
            for j in range(3):
                if i == j:
                    R[i, j] = (
                        math.cos(theta)
                        + (1 - math.cos(theta)) * r_rT[i, j]
                        + math.sin(theta) * r_cross[i, j]
                    )
                else:
                    R[i, j] = (1 - math.cos(theta)) * r_rT[i, j] + math.sin(
                        theta
                    ) * r_cross[i, j]


@cuda.jit(device=True)
def _rodrigues_get_rho(rho, A):  # sourcery skip: use-itertools-product
    for i in range(3):
        for j in range(3):
            for k in range(3):
                if k == 0 and i == 2 and j == 1:
                    rho[k] = A[i, j]
                if k == 1 and i == 0 and j == 2:
                    rho[k] = A[i, j]
                if k == 2 and i == 1 and j == 0:
                    rho[k] = A[i, j]


@cuda.jit(device=True)
def _rodrigues_sign(r):
    n_r = cdvec.cuda_vec_norm(r, 3)
    if (
        n_r == pi
        and (r[0] == r[1] == 0 and r[2] < 0)
        or ((r[0] == 0 and r[1] < 0) or r[0] < 0)
    ):
        # return  -r
        cdvec.cuda_vec_scalarMul(r, -1, r, 3)
    else:
        # return r
        pass


@cuda.jit(device=True)
def _rodrigues_atan2(y, x, CONS_ZEROS):
    """_summary_
    Calculate atan2(y, x) with a special treatment for the case where x = 0.
    Args:
        y (_type_): _description_
        x (_type_): _description_
        CONS_ZEROS (_type_): _description_

    Returns:
        float: _description_
    """
    if x > 0:
        return atan(y / x)
    elif x < 0:
        return atan(y / x) + pi
    elif numerical_limit_zero(x, CONS_ZEROS) and y > 0:
        return pi / 2
    elif numerical_limit_zero(x, CONS_ZEROS) and y < 0:
        return -pi / 2


@cuda.jit(device=True)
def numerical_limit_zero(x, CONS_ZEROS):
    """_summary_
    Check if x is close to zero.
    Args:
        x (_type_): _description_
        CONS_ZEROS (_type_): _description_

    Returns:
        _type_: _description_
    """
    return -CONS_ZEROS < x < CONS_ZEROS


@cuda.jit(device=True)
def numerical_limit_value(x, val, CONS_ZEROS):
    """_summary_
    Check if x is close to val.
    Args:
        x (_type_): _description_
        val (_type_): _description_
        CONS_ZEROS (_type_): _description_

    Returns:
        _type_: _description_
    """
    return (val + CONS_ZEROS) > x > (val - CONS_ZEROS)


@cuda.jit(device=True)
def cuda_RotMat_To_RotVec(R, r, CONS_ZEROS, float_type):
    """_summary_
    Convert rotation matrix to Rodrigues rotation vector.
    See : Tomasi, C., 2013. Vector representation of rotations. Computer Science, 527.
    Args:
        R (matrix): (3,3) array, rotation matrix
        r (vector): (3,1) array, Rodrigues rotation vector
        CONS_ZEROS (float): system zero
        float_type (_type_): np.float64
    """
    dim33 = (3, 3)
    A = cuda.local.array(dim33, dtype=float_type)
    R_T = cuda.local.array(dim33, dtype=float_type)
    rho = cuda.local.array(3, dtype=float_type)
    u = cuda.local.array(3, dtype=float_type)

    # Compute A = (R - R.T) / 2
    cdmat.cuda_mat_transpose(R, R_T, dim33)
    cdmat.cuda_mat_sub(R, R_T, A, dim33)
    cdmat.cuda_mat_scalarDiv(A, 2, A, dim33)

    #  Compute rho = np.array([A[2, 1], A[0, 2], A[1, 0]])
    _rodrigues_get_rho(rho, A)
    s = cdvec.cuda_vec_norm(rho, 3)

    # Compute c
    # c = (sum(diag(R)) - 1) / 2
    c = (R[0, 0] + R[1, 1] + R[2, 2] - 1) / 2

    if numerical_limit_zero(s, CONS_ZEROS) and numerical_limit_value(c, 1, CONS_ZEROS):
        # r = np.zeros(3)
        cdvec.cuda_vec_zeros(u, 3)
        cdvec.cuda_vec_copy(r, u, 3)
        # return r

    elif numerical_limit_zero(s, CONS_ZEROS) and numerical_limit_value(
        c, -1, CONS_ZEROS
    ):
        #  Define locals arrays
        I = cdmat.cuda_eye(cuda.local.array(dim33, dtype=float_type), 3)
        V = cuda.local.array(dim33, dtype=float_type)
        v = cuda.local.array(3, dtype=float_type)

        # Compute V = R + np.eye(3)
        cdmat.cuda_mat_add(R, I, V, dim33)

        # Sum on V columns
        Vcol_sum = cuda.local.array(3, dtype=float_type)
        # Vcol_sum.sum(axis=0)
        cdmat.cuda_mat_sum(V, Vcol_sum, 0, dim33)
        for i in range(3):
            if Vcol_sum[i] != 0:
                # v = V[:, i]
                cdvec.cuda_vec_copy(v, V[:, i], 3)
                break
            else:
                continue

        # u = v / norm(v)
        cdvec.cuda_vec_get_normalized(v, u, 3)
        # u = u * np.pi
        cdvec.cuda_vec_scalarMul(u, pi, u, 3)
        # r = sign(u * np.pi)
        _rodrigues_sign(u)
        cdvec.cuda_vec_copy(r, u, 3)
        # return r
    else:
        # u = rho / s
        cdvec.cuda_vec_scalarDiv(rho, s, u, 3)
        theta = _rodrigues_atan2(s, c, CONS_ZEROS)
        # r = u * theta
        cdvec.cuda_vec_scalarMul(u, theta, r, 3)
        # return r


@cuda.jit(device=True)
def cuda_compose_RT(r1, t1, r2, t2, r3, t3, CONS_ZEROS, float_type):
    """_summary_
    Compose rotation and translation vectors from Rodrigues formalism.
        rvec3=rodrigues^(-1)(rodrigues(rvec2)⋅rodrigues(rvec1)),
        tvec3=rodrigues(rvec2)⋅𝚝𝚟𝚎𝚌𝟷+tvec2
    where rodrigues function denotes transformation from vector to rotation matrix.
    and rodrigues^(-1) denotes transformation from rotation matrix to vector.
    See : https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html#ga044b03d81f99370db05c52c3d0b46599

    Args:
        r1 (vector): input rotation vector
        t1 (vector): input translation vector
        r2 (vector): input rotation vector
        t2 (vector): input translation vector
        r3 (vector): output rotation vector
        t3 (vector): output translation vector
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """

    R1 = cuda.local.array((3, 3), dtype=float_type)
    R2 = cuda.local.array((3, 3), dtype=float_type)
    R3 = cuda.local.array((3, 3), dtype=float_type)

    cuda_RotVec_To_RotMat(r1, R1, CONS_ZEROS, float_type)
    cuda_RotVec_To_RotMat(r2, R2, CONS_ZEROS, float_type)

    cdmat.cuda_mat_mul(R2, R1, R3, (3, 3), (3, 3))

    cuda_RotMat_To_RotVec(R3, r3, CONS_ZEROS, float_type)

    cdmat.cuda_mat_vecMul(R2, t1, t3, (3, 3))
    cdvec.cuda_vec_add(t3, t2, t3, 3)


@cuda.jit(device=True)
def cuda_invert_RT(r, t, ri, ti, CONS_ZEROS, float_type):
    """_summary_
    Invert rotation and translation vectors from Rodrigues formalism.

    Ti = -cv2.Rodrigues(-R)[0].dot(T)
    Ri = -R

    Args:
        r (vector): _description_
        t (vector): _description_
        ri (vector): _description_
        ti (vector): _description_
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """
    R = cuda.local.array((3, 3), dtype=float_type)
    m_r = cuda.local.array(3, dtype=float_type)
    cdvec.cuda_vec_scalarMul(r, -1, m_r, 3)
    cuda_RotVec_To_RotMat(m_r, R, CONS_ZEROS, float_type)
    cdmat.cuda_mat_vecMul(R, t, ti, (3, 3))
    cdvec.cuda_vec_scalarMul(ti, -1, ti, 3)
    cdvec.cuda_vec_scalarMul(r, -1, ri, 3)


@cuda.jit(device=True)
def cuda_apply_RT(P, r, t, Q, size, CONS_ZEROS, float_type):
    """_summary_
    Apply rotation and translation vectors to 3D points.
    Args:
        P (matrix): _description_
        r (vector): _description_
        t (vector): _description_
        Q (matrix): _description_
        size (int): number of 3D points (size, 3).
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """
    for i in range(size):
        cuda_apply_RT_point(P[i, :], r, t, Q[i, :], CONS_ZEROS, float_type)


@cuda.jit(device=True)
def cuda_apply_RT_point(P, r, t, Q, CONS_ZEROS, float_type):
    """_summary_
    Apply rotation and translation vectors to a 3D point.
    Args:
        P (_type_): _description_
        r (_type_): _description_
        t (_type_): _description_
        Q (_type_): _description_
        CONS_ZEROS (_type_): system zero
        float_type (_type_): _description_
    """
    R = cuda.local.array((3, 3), dtype=float_type)
    cuda_RotVec_To_RotMat(r, R, CONS_ZEROS, float_type)
    cdmat.cuda_mat_vecMul(R, P, Q, (3, 3))
    cdvec.cuda_vec_add(t, Q, Q, 3)


@cuda.jit(device=True)
def cuda_project_point(
    Pw, r, t, camera_matrix, distorsion_coeff, pi, CONS_ZEROS, float_type
):
    """_summary_
    Project 3D point to 2D image point.
    Args:
        Pw (vector): 3D point expressed in world coordinates, in meter.
        r (vector): Rodriques rotation vector, allow change of coordinate system from world to camera.
        t (vector): Translation vector, allow change of coordinate system from world to camera.
        camera_matrix (matrix): camera matrix (3, 3) give the intrinsic parameters of the camera.
        distorsion_coeff (vector): _description_
        pi (vector): 2D point expressed in image coordinates, in pixel.
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """
    Pc = cuda.local.array(3, dtype=float_type)

    # Change reference frame from world to camera
    cuda_apply_RT_point(Pw, r, t, Pc, CONS_ZEROS, float_type)

    # Perspective projection point to image
    pi[0] = Pc[0] / Pc[2]
    pi[1] = Pc[1] / Pc[2]

    # Apply distortion
    cuda_apply_distortion(pi, distorsion_coeff, pi)

    # Project 3D point from camera reference frame to image reference frame plane
    pi[0] = camera_matrix[0, 0] * pi[0] + camera_matrix[0, 2]
    pi[1] = camera_matrix[1, 1] * pi[1] + camera_matrix[1, 2]


@cuda.jit(device=True)
def cuda_project_points(
    Pw, r, t, camera_matrix, distorsion_coeff, pi, size, CONS_ZEROS, float_type
):
    """_summary_
    Project 3D points to 2D image points.
    Args:
        Pw (matrix): _description_
        r (vector): _description_
        t (vector): _description_
        camera_matrix (matrix): _description_
        distorsion_coeff (vector): _description_
        pi (matrix): _description_
        size (int): _description_
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """

    for i in range(size):
        cuda_project_point(
            Pw[i, :],
            r,
            t,
            camera_matrix,
            distorsion_coeff,
            pi[i, :],
            CONS_ZEROS,
            float_type,
        )


@cuda.jit(device=True)
def cuda_apply_distortion(p, distorsion_coeff, pd):
    """_summary_
    Apply distortion to 2D image point.
    distorsion_coeff = (k1, k2, p1, p2, k3)

    See : https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html#ga3207604e4b1a1758aa66acb6ed5aa65d
        and https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html

    Args:
        p (vector): 2D point
        camera_matrix (matrix): _description_
        distorsion_coeff (vector): _description_
        pd (vector): distorted 2D point
        CONS_ZEROS (float): system zero
        float_type (_type_): _description_
    """
    r = sqrt(p[0] ** 2 + p[1] ** 2)

    pd[0] = (
        p[0]
        * (
            1
            + distorsion_coeff[0] * r**2
            + distorsion_coeff[1] * r**4
            + distorsion_coeff[4] * r**6
        )
        + 2 * distorsion_coeff[2] * p[0] * p[1]
        + distorsion_coeff[3] * (r**2 + 2 * p[0] ** 2)
    )
    pd[1] = (
        p[1]
        * (
            1
            + distorsion_coeff[0] * r**2
            + distorsion_coeff[1] * r**4
            + distorsion_coeff[4] * r**6
        )
        + distorsion_coeff[2] * (r**2 + 2 * p[1] ** 2)
        + 2 * distorsion_coeff[3] * p[0] * p[1]
    )
