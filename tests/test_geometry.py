import sys
import time
from math import pi

import cv2
import numpy as np
import tqdm
from numba import cuda

sys.path.append("../")
from cdmath import geometry as cdgeo
from cdmath import matrix as cdmat


class TestRodrigues:
    def test_cuda_RotVec_To_RotMat(self):
        threads_per_block = 1
        blocks_per_grid = 1

        CONS_ZEROS = sys.float_info.epsilon
        v = np.array([np.pi / 4, 0, 0])
        vd = cuda.to_device(v)

        R = np.zeros((3, 3))
        Rd = cuda.to_device(R)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_RotVec_To_RotMat_kernel(v, R, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                cdgeo.cuda_RotVec_To_RotMat(v, R, CONS_ZEROS, float_type)

        cuda.synchronize()
        cuda_RotVec_To_RotMat_kernel[blocks_per_grid, threads_per_block](
            vd, Rd, CONS_ZEROS
        )
        cuda.synchronize()

        print(f"Cuda - Rotation matrix value = {Rd.copy_to_host()}")
        print(f"Opencv - Rotation matrix value = {cv2.Rodrigues(v)[0]}")
        assert np.allclose(Rd.copy_to_host(), cv2.Rodrigues(v)[0])

    def test_cuda_RotMat_To_RotVec(self):

        threads_per_block = 1
        blocks_per_grid = 1

        CONS_ZEROS = sys.float_info.epsilon
        v = np.zeros(3)
        vd = cuda.to_device(v)

        R = np.random.rand(3, 3)
        r = cv2.Rodrigues(R)[0]
        R = cv2.Rodrigues(r)[0]
        Rd = cuda.to_device(R)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_RotMat_To_RotVec_kernel(R, v, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                cdgeo.cuda_RotMat_To_RotVec(R, v, CONS_ZEROS, float_type)

        cuda.synchronize()
        cuda_RotMat_To_RotVec_kernel[blocks_per_grid, threads_per_block](
            Rd, vd, CONS_ZEROS
        )
        cuda.synchronize()

        print(f"Cuda - Rodrigues vector value = {vd.copy_to_host()}")
        print(f"Opencv - Rodrigues vector value = {cv2.Rodrigues(R)[0].reshape(3)}")
        assert np.allclose(vd.copy_to_host(), cv2.Rodrigues(R)[0].reshape(3))


class TestMatrix:
    def test_cuda_mat_vecMul(self):
        threads_per_block = 1
        blocks_per_grid = 1

        v = np.random.rand(3)
        vd = cuda.to_device(v)

        A = np.random.rand(3, 3)
        Ad = cuda.to_device(A)

        M = np.zeros(3)
        Md = cuda.to_device(M)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_mat_vecMul_kernel(A, v, M):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            for _ in range(start, 1, stride):
                dim33 = (3, 3)
                cdmat.cuda_mat_vecMul(A, v, M, dim33)

        cuda.synchronize()
        cuda_mat_vecMul_kernel[blocks_per_grid, threads_per_block](Ad, vd, Md)
        cuda.synchronize()

        print(f"Cuda - matrix-vector multiply value = {Md.copy_to_host()}")
        print(f"Numpy - matrix-vector multiply value  = {A.dot(v)}")
        assert np.allclose(Md.copy_to_host(), A.dot(v))

    def test_cuda_mat_mul(self):
        threads_per_block = 1
        blocks_per_grid = 1

        A = np.random.rand(3, 3)
        Ad = cuda.to_device(A)

        B = np.random.rand(3, 3)
        Bd = cuda.to_device(B)

        M = np.zeros((3, 3))
        Md = cuda.to_device(M)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_mat_mul_kernel(A, B, M):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            for _ in range(start, 1, stride):
                dim33 = (3, 3)
                cdmat.cuda_mat_mul(A, B, M, dim33, dim33)

        cuda.synchronize()
        cuda_mat_mul_kernel[blocks_per_grid, threads_per_block](Ad, Bd, Md)
        cuda.synchronize()

        print(f"Cuda - matrix-vector multiply value = {Md.copy_to_host()}")
        print(f"Numpy - matrix-vector multiply value  = {A.dot(B)}")
        assert np.allclose(Md.copy_to_host(), A.dot(B))

    def test_cuda_mat_mul_hadamard(self):
        threads_per_block = 1
        blocks_per_grid = 1

        A = np.random.rand(3, 3)
        Ad = cuda.to_device(A)

        B = np.random.rand(3, 3)
        Bd = cuda.to_device(B)

        M = np.zeros((3, 3))
        Md = cuda.to_device(M)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_mat_mul_hadamard_kernel(A, B, M):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            for _ in range(start, 1, stride):
                dim33 = (3, 3)
                cdmat.cuda_mat_mul_hadamard(A, B, M, dim33)

        cuda.synchronize()
        cuda_mat_mul_hadamard_kernel[blocks_per_grid, threads_per_block](Ad, Bd, Md)
        cuda.synchronize()

        print(f"Cuda - matrix-vector multiply value = {Md.copy_to_host()}")
        print(f"Numpy - matrix-vector multiply value  = {A*B}")
        assert np.allclose(Md.copy_to_host(), A * B)


class TestRT:
    @staticmethod
    def cpu_compose_RT(R1, T1, R2, T2):
        """
        Composed 2 RT transformations
        """
        R, T = cv2.composeRT(R1, T1, R2, T2)[:2]
        return R.flatten(), T.flatten()

    @staticmethod
    def cpu_invert_RT(R, T):
        """
        Inverts a RT transformation
        """
        Ti = -cv2.Rodrigues(-R)[0].dot(T)
        return -R, Ti

    @staticmethod
    def cpu_apply_RT(P, R, T):
        """
        Applies RT transformation to 3D points P.
        """
        P = cv2.Rodrigues(R)[0].dot(P.T).T
        P += T.T
        return P

    @staticmethod
    def pack_RT(r, t):
        return np.concatenate((r, t)).flatten()

    def test_cuda_compose_RT(self):
        threads_per_block = 1
        blocks_per_grid = 1

        r1 = cv2.Rodrigues(cv2.Rodrigues(np.random.rand(3))[0])[0].flatten()
        t1 = np.random.rand(3)
        r1d = cuda.to_device(r1)
        t1d = cuda.to_device(t1)

        r2 = cv2.Rodrigues(cv2.Rodrigues(np.random.rand(3))[0])[0].flatten()
        t2 = np.random.rand(3)
        r2d = cuda.to_device(r2)
        t2d = cuda.to_device(t2)

        r3 = np.zeros(3)
        t3 = np.zeros(3)
        r3d = cuda.to_device(r3)
        t3d = cuda.to_device(t3)

        CONS_ZEROS = sys.float_info.epsilon

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_compose_RT_kernel(r1, t1, r2, t2, r3, t3, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                dim33 = (3, 3)
                cdgeo.cuda_compose_RT(r1, t1, r2, t2, r3, t3, CONS_ZEROS, float_type)

        cuda.synchronize()
        cuda_compose_RT_kernel[blocks_per_grid, threads_per_block](
            r1d, t1d, r2d, t2d, r3d, t3d, CONS_ZEROS
        )
        cuda.synchronize()

        print(f"Cuda - compose_RT r= {r3d.copy_to_host()}\tt= {t3d.copy_to_host()}")

        r, t = self.cpu_compose_RT(r1, t1, r2, t2)
        print(f"Opencv - compose_RT r={r}\tt={t}")

        rr = r3d.copy_to_host()
        tt = t3d.copy_to_host()

        assert np.allclose(self.pack_RT(rr, tt), self.pack_RT(r, t))

    def test_cuda_invert_RT(self):

        threads_per_block = 1
        blocks_per_grid = 1

        r = np.random.rand(3)
        t = np.random.rand(3)
        rd = cuda.to_device(r)
        td = cuda.to_device(t)

        ri = np.zeros(3)
        ti = np.zeros(3)
        rid = cuda.to_device(ri)
        tid = cuda.to_device(ti)

        CONS_ZEROS = sys.float_info.epsilon

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_invert_RT_kernel(r, t, ri, ti, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                dim33 = (3, 3)
                cdgeo.cuda_invert_RT(r, t, ri, ti, CONS_ZEROS, float_type)

        cuda.synchronize()
        cuda_invert_RT_kernel[blocks_per_grid, threads_per_block](
            rd, td, rid, tid, CONS_ZEROS
        )
        cuda.synchronize()

        print(f"Cuda - Invert_RT r= {rid.copy_to_host()}\tt= {tid.copy_to_host()}")
        rri, tti = self.cpu_invert_RT(r, t)
        print(f"Opencv - Invert_RT r={rri}\tt={tti}")

        rr = rid.copy_to_host()
        tt = tid.copy_to_host()

        assert np.allclose(self.pack_RT(rr, tt), self.pack_RT(rri, tti))

    def test_cuda_apply_RT(self):

        threads_per_block = 1
        blocks_per_grid = 1

        r = np.random.rand(3)
        t = np.random.rand(3)
        rd = cuda.to_device(r)
        td = cuda.to_device(t)

        size = 1000
        P = np.random.rand(size, 3)
        Pd = cuda.to_device(P)

        Q = np.zeros_like(P)
        Qd = cuda.to_device(Q)

        CONS_ZEROS = sys.float_info.epsilon

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_apply_RT_kernel(P, Q, r, t, size, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                cdgeo.cuda_apply_RT(P, r, t, Q, size, CONS_ZEROS, float_type)

        cuda.synchronize()
        cuda_apply_RT_kernel[blocks_per_grid, threads_per_block](
            Pd, Qd, rd, td, size, CONS_ZEROS
        )
        cuda.synchronize()

        Q_cuda = Qd.copy_to_host()
        Q_opencv = self.cpu_apply_RT(P, r, t)

        print(f"Cuda - Apply_RT Q= {Q_cuda}")
        print(f"Opencv - Apply_RT Q={Q_opencv}")

        assert np.allclose(Q_cuda, Q_opencv)

    def test_cuda_apply_RT_point(self):

        threads_per_block = 256
        blocks_per_grid = 512

        r = np.random.rand(3)
        t = np.random.rand(3)
        rd = cuda.to_device(r)
        td = cuda.to_device(t)

        size = int(1e7)
        P = np.random.rand(size, 3)
        Pd = cuda.to_device(P)

        Q = np.zeros_like(P)
        Qd = cuda.to_device(Q)

        CONS_ZEROS = sys.float_info.epsilon
        Finished = cuda.managed_array(size, dtype=bool)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_apply_RT_point_kernel(P, Q, r, t, size, CONS_ZEROS, Finished):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for i in range(start, size, stride):
                cdgeo.cuda_apply_RT_point(
                    P[i, :], r, t, Q[i, :], CONS_ZEROS, float_type
                )
                Finished[i] = True

        cuda.synchronize()
        t0 = time.time()
        cuda_apply_RT_point_kernel[blocks_per_grid, threads_per_block](
            Pd, Qd, rd, td, size, CONS_ZEROS, Finished
        )
        elapse_time_cuda = time.time() - t0

        with tqdm.tqdm(total=size, colour="red") as pbar:
            progress = 0
            while progress < size:
                progress = int(np.sum(Finished))
                time.sleep(0.2)
                old_value = pbar.n
                new_value = progress - old_value
                if new_value != old_value:
                    pbar.update(new_value)

        cuda.synchronize()

        Q_cuda = Qd.copy_to_host()

        t0 = time.time()
        Q_opencv = self.cpu_apply_RT(P, r, t)
        elapse_time_opencv = time.time() - t0

        print(f"Cuda - Apply_RT Q= {Q_cuda}")
        print(f"Opencv - Apply_RT Q={Q_opencv}")

        print(f"Cuda - Elapse time = {elapse_time_cuda}")
        print(f"Opencv - Elapse time = {elapse_time_opencv}")

        assert np.allclose(Q_cuda, Q_opencv)


class TestProjection:
    def test_cuda_project_points(self):

        threads_per_block = 1
        blocks_per_grid = 1

        fx = fy = 1.0
        cx = cy = 0.0
        camera_matrix = np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])
        distorsion_coeff = np.zeros(5)

        r = np.random.rand(3)
        t = np.random.rand(3)
        rd = cuda.to_device(r)
        td = cuda.to_device(t)

        size = 100
        Pw = np.random.rand(size, 3)
        Pwd = cuda.to_device(Pw)

        pi = np.zeros((size, 2))
        pid = cuda.to_device(pi)

        CONS_ZEROS = sys.float_info.epsilon

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_project_points_kernel(Pw, pi, r, t, size, CONS_ZEROS):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for _ in range(start, 1, stride):
                cdgeo.cuda_project_points(
                    Pw,
                    r,
                    t,
                    camera_matrix,
                    distorsion_coeff,
                    pi,
                    size,
                    CONS_ZEROS,
                    float_type,
                )

        cuda.synchronize()
        cuda_project_points_kernel[blocks_per_grid, threads_per_block](
            Pwd, pid, rd, td, size, CONS_ZEROS
        )
        cuda.synchronize()

        pi_cuda = pid.copy_to_host()
        pi_opencv = cv2.projectPoints(Pw, r, t, camera_matrix, distorsion_coeff)[
            0
        ].reshape(-1, 2)

        print(f"Cuda - Apply_RT Q= {pi_cuda}")
        print(f"Opencv - Apply_RT Q={pi_opencv}")

        assert np.allclose(pi_cuda, pi_opencv)

    def test_cuda_project_point(self):

        threads_per_block = 256
        blocks_per_grid = 512

        fx = fy = np.random.rand()
        cx = cy = 0.0
        camera_matrix = np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])

        camera_matrix = np.array(
            [
                [5903.0523293885735, 0.0, 2684.685075209784],
                [0.0, 5903.0523293885735, 1482.819018296687],
                [0.0, 0.0, 1.0],
            ]
        ).reshape(3, 3)

        distorsion_coeff = np.array(
            [[-0.13405912738297218, 0.07940269451799592, 0.0, 0.0, 0.8177373151770626]]
        ).reshape(5)

        r = np.random.rand(3)
        t = np.random.rand(3)
        rd = cuda.to_device(r)
        td = cuda.to_device(t)

        size = int(1e7)
        Pw = np.random.rand(size, 3)
        Pwd = cuda.to_device(Pw)

        pi = np.zeros((size, 2))
        pid = cuda.to_device(pi)

        CONS_ZEROS = sys.float_info.epsilon
        Finished = cuda.managed_array(size, dtype=bool)

        @cuda.jit(debug=False)  # TURN TO True for debugging
        def cuda_project_point_kernel(Pw, pi, r, t, size, CONS_ZEROS, Finished):
            tx = cuda.threadIdx.x  # this is the unique thread ID within a 1D block
            ty = (
                cuda.blockIdx.x
            )  # Similarly, this is the unique block ID within the 1D grid

            block_size = cuda.blockDim.x  # number of threads per block
            grid_size = cuda.gridDim.x  # number of blocks in the grid

            start = tx + ty * block_size
            stride = block_size * grid_size

            float_type = np.float64
            for i in range(start, size, stride):
                cdgeo.cuda_project_point(
                    Pw[i, :],
                    r,
                    t,
                    camera_matrix,
                    distorsion_coeff,
                    pi[i, :],
                    CONS_ZEROS,
                    float_type,
                )
                Finished[i] = True

        cuda.synchronize()
        cuda_project_point_kernel[blocks_per_grid, threads_per_block](
            Pwd, pid, rd, td, size, CONS_ZEROS, Finished
        )
        cuda.synchronize()
        t0 = time.time()
        cuda_project_point_kernel[blocks_per_grid, threads_per_block](
            Pwd, pid, rd, td, size, CONS_ZEROS, Finished
        )
        elapse_time_cuda = time.time() - t0

        with tqdm.tqdm(total=size, colour="red") as pbar:
            progress = 0
            while progress < size:
                progress = int(np.sum(Finished))
                time.sleep(0.2)
                old_value = pbar.n
                new_value = progress - old_value
                if new_value != old_value:
                    pbar.update(new_value)

        cuda.synchronize()

        pi_cuda = pid.copy_to_host()

        t0 = time.time()
        pi_opencv = cv2.projectPoints(Pw, r, t, camera_matrix, distorsion_coeff)[
            0
        ].reshape(-1, 2)
        elapse_time_opencv = time.time() - t0

        print(f"Cuda - Apply_RT Q= {pi_cuda}")
        print(f"Opencv - Apply_RT Q={pi_opencv}")

        print(f"Cuda - Elapse time = {elapse_time_cuda}")
        print(f"Opencv - Elapse time = {elapse_time_opencv}")
        print(f"Performance = {elapse_time_opencv / elapse_time_cuda}")

        assert np.allclose(pi_cuda, pi_opencv)


if __name__ == "__main__":
    test_proj = TestProjection()
    test_proj.test_cuda_project_point()
